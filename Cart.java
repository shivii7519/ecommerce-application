package npciPro;

import java.util.HashMap;

public class Cart {
	private HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();

	public void push(int productId, int quantity) {
		map.put(productId, quantity);
	}

	public HashMap<Integer, Integer> getMap() {
		return map;
	}
}